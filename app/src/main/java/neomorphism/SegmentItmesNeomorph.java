package neomorphism;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

import com.chinodev.androidneomorphframelayout.NeomorphFrameLayout;

import java.util.ArrayList;

import it.ditz.app.R;

public class SegmentItmesNeomorph extends FrameLayout implements View.OnClickListener {

    LinearLayout containerView;
    ArrayList<View> items = new ArrayList<>();
    View lastSelected;
    int currentSelect = 0;
    public OnSegmentsItemSelected onSegmentsItemSelected;

    public SegmentItmesNeomorph(Context context) {
        super(context);
        init(context, null, 0);
    }

    public SegmentItmesNeomorph(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public SegmentItmesNeomorph(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        getAttrs(context, attrs);
    }

    public void getAttrs(Context context, AttributeSet attrs) {
        int defaultElevation = (int) context.getResources().getDimension(R.dimen.neomorph_view_elevation);
        int defaultCornerRadius = (int) context.getResources().getDimension(R.dimen.neomorph_view_corner_radius);

        if (attrs != null) {
            //get attrs array
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SegmentItmesNeomorph);
            //get all attributes
            CharSequence[] values =  a.getTextArray(R.styleable.SegmentItmesNeomorph_neomorphsegment_items);
            CharSequence[] valuesImages =  a.getTextArray(R.styleable.SegmentItmesNeomorph_neomorphsegment_items_icons);
            LayoutInflater.from(context).inflate(R.layout.segments_container,this,true);
            //this.addView(containerView);
            containerView = findViewById(R.id.container);
            int index = 0;
            for(CharSequence val : values){
                View view = LayoutInflater.from(context).inflate(R.layout.segments_items,containerView,false);
                //View view = findViewById(R.id.containerItem);
                MyNeomorphFrameLayout neomorphFrameLayout = view.findViewById(R.id.segmentNeomorph);
                TextView textView = view.findViewById(R.id.segmentText);
                textView.setText(val.toString());
                AppCompatImageView appCompatImageView = view.findViewById(R.id.segmentIcon);
                if(valuesImages != null) {
                    appCompatImageView.setVisibility(View.VISIBLE);
                    int resID = getResources().getIdentifier(valuesImages[index].toString(), "drawable", context.getPackageName());
                    appCompatImageView.setImageResource(resID);
                }
                neomorphFrameLayout.setTag(index);
                view.setTag(index);
                containerView.addView(view);
                items.add(view);
                view.setOnClickListener(this);
                neomorphFrameLayout.setOnClickListener(this);
                index ++;
            }

            currentSelect = 0;
            udpdateSegmentsView();
            a.recycle();
        }
    }

    @Override
    public void onClick(View view) {
        currentSelect = (int) view.getTag();
        udpdateSegmentsView();
        if(onSegmentsItemSelected != null)
            onSegmentsItemSelected.onItemSelected(currentSelect);
    }

    private void udpdateSegmentsView(){
        if(currentSelect<items.size()){
            if(lastSelected != null){
                MyNeomorphFrameLayout neomorphFrameLayout = lastSelected.findViewById(R.id.segmentNeomorph);
                TextView textView = lastSelected.findViewById(R.id.segmentText);
                AppCompatImageView appCompatImageView = lastSelected.findViewById(R.id.segmentIcon);
                neomorphFrameLayout.setVisibility(INVISIBLE);
                textView.setTextColor(getResources().getColor(R.color.colorTextGray));
                appCompatImageView.setColorFilter(getResources().getColor(R.color.colorTextGray));
            }

            View current = items.get(currentSelect);
            MyNeomorphFrameLayout neomorphFrameLayout = current.findViewById(R.id.segmentNeomorph);
            TextView textView = current.findViewById(R.id.segmentText);
            AppCompatImageView appCompatImageView = current.findViewById(R.id.segmentIcon);
            neomorphFrameLayout.setVisibility(VISIBLE);
            textView.setTextColor(getResources().getColor(R.color.colorWhite));
            appCompatImageView.setColorFilter(getResources().getColor(R.color.colorWhite));
            lastSelected = current;
        }
    }

    public interface OnSegmentsItemSelected{
        public void onItemSelected(int index);
    }
}
