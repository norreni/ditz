package tools;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.util.Log;


import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

import it.MyApplication;

public class Utils {

    private static String PRIVATE_PREFS_KEY = "DITZ";

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MyApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static <T> void removeObject(Context context, Class<T> classe) {


        SharedPreferences prefs = context.getSharedPreferences(PRIVATE_PREFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        Log.d("map values key", classe.getSimpleName());
        prefsEditor.remove(classe.getSimpleName());
        prefsEditor.commit();

    }

    public static void storeObject(Context context, Object obj) {
        SharedPreferences prefs = context.getSharedPreferences(PRIVATE_PREFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        //if(obj!=null){
        Gson gson = new Gson();
        // if (Application.isDEBUG())logger.info("salvo con chiave "+obj.getClass().getSimpleName());
        String json = gson.toJson(obj);
        //   if (Application.isDEBUG())logger.info("il valore "+json);
        prefsEditor.putString(obj.getClass().getSimpleName(), json);
        //}
        prefsEditor.commit();
    }

    public static <T> T getObjectFromPrefs(Context context, Class<T> classe) {
        SharedPreferences prefs = context.getSharedPreferences(PRIVATE_PREFS_KEY, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        // if (Application.isDEBUG())logger.info("cerco con chiave "+classe.getSimpleName());
        String json = prefs.getString(classe.getSimpleName(), "");
        //  if (Application.isDEBUG())logger.info("leggo il valore "+json);
        if (json == null || json.equals("")) return null;
        T obj = gson.fromJson(json, classe);
        return obj;
    }

    public static String getTimeFormat(long time, String forrmat){
        SimpleDateFormat formatter = new SimpleDateFormat(forrmat);
        try
        {

            Date date = new Date(time);
            return formatter.format(date);

        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        return "";
    }
}