package tools;

public class IntentsParameters {
    public static class Request{
        public static int REQUEST_COARSE_LOCATION = 2001;
        public static int REQUEST_CHOOSE_ADDRESS_ON_CAP = 2002;
        public static String REQUEST_NOTIFICATION_RECIVER = "notificationReciver";
    }

    public static class Result{
        public static int REQUEST_CONNECT_DEVICE = 1000;
    }

    public static class Extra{
        public static String EXTRA_KEY_ADDRESS_LATITUDE = "lat";
        public static String EXTRA_KEY_ADDRESS_LONGITUDE = "lng";
        public static String EXTRA_KEY_ADDRESS_NAME = "address";
    }
}
