package tools;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chaos.view.PinView;

import java.util.Random;

import it.ditz.app.R;

public class MyDialog {public MyDialogOption option;
    public Dialog dialog;
    Context context;
    int resLayout;

    //create dialog loadgin
    public MyDialog(Context context){
        MyDialogOption option = new MyDialogOption();
        option.setLoop(false);

        //this.option = option;
        this.context = context;
        this.resLayout = R.layout.dialog_loading;
        init();
    }

    public MyDialog(Context context, int resLayout,@Nullable MyDialogOption option){
        this.option = option;
        this.context = context;
        this.resLayout = resLayout;
        init();
    }

    private void init(){
        if(this.option == null)
            this.option = new MyDialogOption();

        dialog = new Dialog(this.context, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(resLayout);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(this.option.isCancelable());
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        /*dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        dialog.getWindow().setStatusBarColor(Color.TRANSPARENT);
        dialog.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);*/

        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        dialog.getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        dialog.getWindow().setStatusBarColor(Color.TRANSPARENT);

        updateContentView();
    }

    public void setMessage(String message){
        if(option.getResMessage()!=-1){
            TextView textView = dialog.findViewById(option.getResMessage());
            if(textView!=null)
                textView.setText(message);
        }
    }

    public void updateContentView(){
        if(option.getResTitle()!=-1){
            TextView textView = dialog.findViewById(option.getResTitle());
            if(textView!=null && !option.getTitle().isEmpty())
                textView.setText(option.getTitle());
        }

        if(option.getResMessage()!=-1){
            TextView textView = dialog.findViewById(option.getResMessage());
            if(textView!=null && !option.getMessage().isEmpty())
                textView.setText(option.getMessage());
        }

        if(option.getResButtonConfirm()!=-1){
            if (dialog.findViewById(option.getResButtonConfirm()) instanceof TextView) {
                TextView textView = dialog.findViewById(option.getResButtonConfirm());
                if (textView != null && !option.getButtonTitleConfirm().isEmpty())
                    textView.setText(option.getButtonTitleConfirm());
            }
        }

        if(option.getResButtonCancel()!=-1){
            if (dialog.findViewById(option.getResButtonCancel()) instanceof TextView) {
                TextView textView = dialog.findViewById(option.getResButtonCancel());
                if (textView != null && !option.getButtonTitleCancel().isEmpty())
                    textView.setText(option.getButtonTitleCancel());
            }
        }

        if(option.getResEditText() != -1){
            if (dialog.findViewById(option.getResEditText()) instanceof EditText) {
                EditText textView = dialog.findViewById(option.getResEditText());
                if (textView != null && !option.getHintEditText().isEmpty())
                    textView.setHint(option.getHintEditText());
            }
        }

        if(option.getOnClickListenerCancel()!=null && option.getResButtonCancel()!=-1){
            View view = dialog.findViewById(option.getResButtonCancel());
            view.setOnClickListener(option.getOnClickListenerCancel());
        }

        if(option.getOnClickListenerConfirm()!=null && option.getResButtonConfirm()!=-1){
            View view = dialog.findViewById(option.getResButtonConfirm());
            view.setOnClickListener(option.getOnClickListenerConfirm());
        }



        if(option.getResOtpTextView()!=-1 && option.getResCodeOtp()!=-1){
            PinView pinView = dialog.findViewById(option.getResOtpTextView());
            pinView.setText("");
            Random rand = new Random();
            final String id = String.format("%04d", rand.nextInt(10000));
            pinView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.toString().length() == 6){
                        if(s.toString().equals(id)){
                            if(option.getOtpEnter()!=null)
                                option.getOtpEnter().onOtpEnter(true);
                        }
                        else {
                            if(option.getOtpEnter()!=null)
                                option.getOtpEnter().onOtpEnter(false);
                        }
                    }
                }
            });
            //TextView textCode = dialog.findViewById(option.getResCodeOtp());
            //textCode.setText(id);
        }
    }

    public void show(){
        dialog.show();
        if(option.getResAnimation()!=-1) {
        }
    }

    public void dismiss(){
        dialog.dismiss();
    }


    public static class MyDialogOption{
        private int resTitle = -1;
        private int resMessage = -1;
        private int resButtonConfirm = -1;
        private int resButtonCancel = -1;
        private int resAnimation = -1;
        private int resCodeOtp = -1;
        private int resOtpTextView = -1;
        private int resEditText = -1;
        private boolean cancelable = true;
        private boolean loop = false;



        private String title = "";
        private String message = "";
        private String buttonTitleConfirm = "";
        private String buttonTitleCancel = "";
        private String hintEditText = "";

        private View.OnClickListener onClickListenerConfirm;
        private View.OnClickListener onClickListenerCancel;
        private OnAnimationComplete onCompleteAnimation;

        public interface OnAnimationComplete{
            public void onComplete();
        }

        private OtpEnter otpEnter;


        public MyDialogOption(){}

        public void setButtonTitleCancel(String buttonTitleCancel) {
            this.buttonTitleCancel = buttonTitleCancel;
        }

        public void setButtonTitleConfirm(String buttonTitleConfirm) {
            this.buttonTitleConfirm = buttonTitleConfirm;
        }

        public void setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setResButtonCancel(int resButtonCancel) {
            this.resButtonCancel = resButtonCancel;
        }

        public void setOnClickListenerCancel(View.OnClickListener onClickListenerCancel) {
            this.onClickListenerCancel = onClickListenerCancel;
        }

        public void setOnClickListenerConfirm(View.OnClickListener onClickListenerConfirm) {
            this.onClickListenerConfirm = onClickListenerConfirm;
        }

        public void setResButtonConfirm(int resButtonConfirm) {
            this.resButtonConfirm = resButtonConfirm;
        }

        public void setResEditText(int resEditText) {
            this.resEditText = resEditText;
        }

        public int getResButtonCancel() {
            return resButtonCancel;
        }

        public int getResButtonConfirm() {
            return resButtonConfirm;
        }

        public int getResMessage() {
            return resMessage;
        }

        public int getResTitle() {
            return resTitle;
        }

        public int getResEditText() {
            return resEditText;
        }

        public View.OnClickListener getOnClickListenerCancel() {
            return onClickListenerCancel;
        }

        public View.OnClickListener getOnClickListenerConfirm() {
            return onClickListenerConfirm;
        }

        public String getButtonTitleCancel() {
            return buttonTitleCancel;
        }

        public String getButtonTitleConfirm() {
            return buttonTitleConfirm;
        }

        public String getMessage() {
            return message;
        }

        public String getTitle() {
            return title;
        }

        public String getHintEditText() {
            return hintEditText;
        }

        public boolean isCancelable() {
            return cancelable;
        }

        public void setResAnimation(int resAnimation) {
            this.resAnimation = resAnimation;
        }

        public void setResCodeOtp(int resCodeOtp) {
            this.resCodeOtp = resCodeOtp;
        }

        public void setResMessage(int resMessage) {
            this.resMessage = resMessage;
        }

        public void setResOtpTextView(int resOtpTextView) {
            this.resOtpTextView = resOtpTextView;
        }

        public void setResTitle(int resTitle) {
            this.resTitle = resTitle;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setHintEditText(String hintEditText) {
            this.hintEditText = hintEditText;
        }

        public int getResAnimation() {
            return resAnimation;
        }

        public int getResCodeOtp() {
            return resCodeOtp;
        }

        public int getResOtpTextView() {
            return resOtpTextView;
        }

        public void setOtpEnter(OtpEnter otpEnter) {
            this.otpEnter = otpEnter;
        }

        public OtpEnter getOtpEnter() {
            return otpEnter;
        }

        public void setLoop(boolean loop) {
            this.loop = loop;
        }

        public void setOnCompleteAnimation(OnAnimationComplete onCompleteAnimation) {
            this.onCompleteAnimation = onCompleteAnimation;
        }

        public OnAnimationComplete getOnCompleteAnimation() {
            return onCompleteAnimation;
        }

        public boolean isLoop() {
            return loop;
        }
    }

    public interface OtpEnter{
        public void onOtpEnter(boolean state);
    }
}

