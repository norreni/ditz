package adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import entities.ShoppingList;
import it.ditz.app.R;
import neomorphism.MyNeomorphFrameLayout;
import tools.Utils;

public class ShoppingListsAdapter extends RecyclerView.Adapter<ShoppingListsAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<ShoppingList> shoppingLists;
    boolean select;
    public ShoppingList selectedShoppingList = null;

    public ShoppingListsAdapter(Context context, ArrayList<ShoppingList> deliveries,boolean select){
        this.mContext = context;
        this.shoppingLists = deliveries;
        this.select = select;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_shopping_list, parent, false);

        int width  = Resources.getSystem().getDisplayMetrics().widthPixels;
        int size = (int) (width * 0.6);

        AbsListView.LayoutParams params = new AbsListView.LayoutParams(size, ViewGroup.LayoutParams.MATCH_PARENT);
        v.setLayoutParams(params);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        TextView txtTitle = holder.view.findViewById(R.id.txtTitle);
        TextView txtItems = holder.view.findViewById(R.id.txtItems);

        AppCompatImageView imageCategory = holder.view.findViewById(R.id.icon_category);
        AppCompatImageView imageWhere = holder.view.findViewById(R.id.icon_where);
        AppCompatImageView imageWhen = holder.view.findViewById(R.id.icon_when);
        AppCompatImageView imageRange = holder.view.findViewById(R.id.icon_range);

        final ShoppingList shoppingList = shoppingLists.get(position);

        txtTitle.setText(Utils.getTimeFormat(shoppingList.getTimeCreate(),"E MMMM dd, hh:mm a"));
        txtItems.setText(shoppingList.getItems());

        MyNeomorphFrameLayout myNeomorphFrameLayout = holder.view.findViewById(R.id.boxContent);
        if(select) {
            myNeomorphFrameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedShoppingList = shoppingList;
                    ShoppingListsAdapter.this.notifyDataSetChanged();
                }
            });

            if(selectedShoppingList == shoppingList)
                myNeomorphFrameLayout.switchShadowType();
            else {
                if(myNeomorphFrameLayout.SHADOW_TYPE == myNeomorphFrameLayout.SHADOW_TYPE_INNER)
                    myNeomorphFrameLayout.switchShadowType();
            }
        }

    }

    @Override
    public int getItemCount() {
        return shoppingLists.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        public MyViewHolder(View v) {
            super(v);
            view = v;
        }
    }


}

