package adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import entities.Delivery;
import it.ditz.app.R;
import tools.Utils;

public class DeliveriesAdapter extends RecyclerView.Adapter<DeliveriesAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<Delivery> deliveries;


    public DeliveriesAdapter(Context context, ArrayList<Delivery> deliveries){
        this.mContext = context;
        this.deliveries = deliveries;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_delivery, parent, false);

        int width  = Resources.getSystem().getDisplayMetrics().widthPixels;
        int size = (int) (parent.getWidth() * 0.7);

        AbsListView.LayoutParams params = new AbsListView.LayoutParams(size, ViewGroup.LayoutParams.MATCH_PARENT);
        v.setLayoutParams(params);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        TextView txtTitle = holder.view.findViewById(R.id.txtTitle);
        TextView txtWhere = holder.view.findViewById(R.id.txtWhere);
        TextView txtWhen = holder.view.findViewById(R.id.txtWhen);
        TextView txtRange = holder.view.findViewById(R.id.txtRange);

        AppCompatImageView imageCategory = holder.view.findViewById(R.id.icon_category);
        AppCompatImageView imageWhere = holder.view.findViewById(R.id.icon_where);
        AppCompatImageView imageWhen = holder.view.findViewById(R.id.icon_when);
        AppCompatImageView imageRange = holder.view.findViewById(R.id.icon_range);

        final Delivery delivery = deliveries.get(position);
        int color = delivery.getType() == Delivery.Type.market ? mContext.getResources().getColor(R.color.colorFood) : mContext.getResources().getColor(R.color.colorPharma);

        imageCategory.setImageResource(delivery.getType() == Delivery.Type.market ? R.drawable.ic_restaurant_menu_black_24dp : R.drawable.ic_local_hospital_black_24dp);
        imageCategory.setColorFilter(color);
        imageWhere.setColorFilter(color);
        imageWhen.setColorFilter(color);
        imageRange.setColorFilter(color);

        txtTitle.setText(delivery.getType() == Delivery.Type.market ? "Sei disponibile per una\nconsegna di generi alimentari" : "Sei disponibile per una\nconsegna di prodotti farmaceutici");
        txtWhere.setText(delivery.getWhereInfo());
        txtWhen.setText(delivery.getTimeExecute("E MMMM dd, hh:mm a"));
        txtRange.setText("Max "+delivery.getLimitPieces()+"pz  Max "+delivery.getLimitPrice()+"€");

    }

    @Override
    public int getItemCount() {
        return deliveries.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View view;
        public MyViewHolder(View v) {
            super(v);
            view = v;
        }
    }


}

