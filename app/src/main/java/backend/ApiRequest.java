package backend;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Source;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import entities.Delivery;
import entities.SessionManager;
import entities.ShoppingList;
import entities.User;
import it.ditz.app.R;
import tools.MyDialog;

public class ApiRequest {
    private static String TAG = "ApiRequest";
    public interface Response{
        public void onComplete(Object value);
        public void onError(Object value);
    }


    public static void sendOtp(final Context context, String phone, Boolean showLoading, final Response response){
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading,null);

        if(showLoading)
            myDialogLoading.dialog.show();
        PhoneAuthProvider.OnVerificationStateChangedCallbacks callback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks(){

            @Override
            public void onVerificationCompleted(@NonNull final PhoneAuthCredential phoneAuthCredential) {
                // myDialogLoading.dialog.dismiss();
                FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        myDialogLoading.dialog.dismiss();
                        if (task.isSuccessful()) {
                            if(response!=null){
                                response.onComplete(phoneAuthCredential);
                            }
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w("OtpVerification", "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                FirebaseAuthInvalidCredentialsException exception = (FirebaseAuthInvalidCredentialsException) task.getException();
                                Toast.makeText(context,exception.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                            }

                            if(response!=null){
                                response.onError(task.getException());
                            }
                        }
                    }
                });
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete(s);
                }
            }
        };
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,// Unit of timeout
                TaskExecutors.MAIN_THREAD,
                callback);        // OnVerificationStateChangedCallbacks
    }


    public static void phoneExist(final Context context,String phone, Boolean showLoading, final Response response) {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading, null);

        if (showLoading)
            myDialogLoading.dialog.show();


        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();

        dbFirebase.collection(DBFirebase.TABLE_USERS.name).whereEqualTo(DBFirebase.TABLE_USERS.column_phone, phone).get(Source.SERVER).addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                myDialogLoading.dialog.dismiss();
                if(queryDocumentSnapshots.size() == 0){
                    if (response != null) {
                        response.onComplete(false);
                    }
                }
                else {
                    if (response != null) {
                        response.onComplete(true);
                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG, "Listen failed.", e);
                myDialogLoading.dialog.dismiss();
                if (response != null) {
                    response.onError(e);
                }
            }
        });
    }

    public static void login(final Context context, String phone, Boolean showLoading, final Response response){
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading,null);

        if(showLoading)
            myDialogLoading.dialog.show();


        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();

        dbFirebase.collection(DBFirebase.TABLE_USERS.name).whereEqualTo(DBFirebase.TABLE_USERS.column_phone,phone).get(Source.SERVER).addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    User user = doc.toObject(User.class);
                    user.setUid(doc.getId());
                    SessionManager.getInstance(context).currentUser = user;
                    user.saveInLocal();
                    myDialogLoading.dialog.dismiss();
                    if(response!=null){
                        response.onComplete(user);
                    }
                    return;
                }
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG, "Listen failed.", e);
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }


    public static void signup(final Context context, final User user, Boolean showLoading, final Response response) {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading, null);

        if (showLoading)
            myDialogLoading.dialog.show();

        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();

        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(DBFirebase.TABLE_USERS.column_firstName, user.getFirstName());
        objectMap.put(DBFirebase.TABLE_USERS.column_lastName, user.getLastName());
        objectMap.put(DBFirebase.TABLE_USERS.column_phone, user.getPhone());
        objectMap.put(DBFirebase.TABLE_USERS.column_address, user.getAddress());
        objectMap.put(DBFirebase.TABLE_USERS.column_address_lat, user.getLatitude());
        objectMap.put(DBFirebase.TABLE_USERS.column_address_lng, user.getLongitude());
        objectMap.put(DBFirebase.TABLE_USERS.column_regisration_tokern, user.getRegistrationToken());

        dbFirebase.collection(DBFirebase.TABLE_USERS.name).document(user.getUid()).set(objectMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                SessionManager.getInstance(context).currentUser = user;
                user.saveInLocal();
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }

    public static void updateToken(final Context context, final User user, Boolean showLoading, final Response response) {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading, null);

        if (showLoading)
            myDialogLoading.dialog.show();

        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();

        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(DBFirebase.TABLE_USERS.column_firstName, user.getFirstName());
        objectMap.put(DBFirebase.TABLE_USERS.column_lastName, user.getLastName());
        objectMap.put(DBFirebase.TABLE_USERS.column_phone, user.getPhone());
        objectMap.put(DBFirebase.TABLE_USERS.column_address, user.getAddress());
        objectMap.put(DBFirebase.TABLE_USERS.column_address_lat, user.getLatitude());
        objectMap.put(DBFirebase.TABLE_USERS.column_address_lng, user.getLongitude());
        objectMap.put(DBFirebase.TABLE_USERS.column_regisration_tokern, user.getRegistrationToken());

        dbFirebase.collection(DBFirebase.TABLE_USERS.name).document(user.getUid()).set(objectMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                SessionManager.getInstance(context).currentUser = user;
                user.saveInLocal();
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }




    public static void createDelivery(final Context context, final Delivery delivery, Boolean showLoading, final Response response) {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading, null);

        if (showLoading)
            myDialogLoading.dialog.show();

        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();


        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_uidOfferer, delivery.getUidOfferer());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_nameOfferer, delivery.getUserOfferer().getFirstName());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_surnameOfferer, delivery.getUserOfferer().getLastName());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_lat, delivery.getUserOfferer().getLatitude());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_lng, delivery.getUserOfferer().getLongitude());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_address, delivery.getUserOfferer().getAddress());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_whereInfo, delivery.getWhereInfo());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_timeCreate, delivery.getTimeCreate());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_timeExecution, delivery.getTimeExecution());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_limitPieces, delivery.getLimitPieces());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_limitPrice, delivery.getLimitPrice());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_type, delivery.getType());
        objectMap.put(DBFirebase.TABLE_DELIVERIES.column_state, delivery.getState());

        dbFirebase.collection(DBFirebase.TABLE_DELIVERIES.name).add(objectMap).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }

    public static void deleteDelivery(final Context context, final Delivery delivery, Boolean showLoading, final Response response) {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading, null);

        if (showLoading)
            myDialogLoading.dialog.show();

        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();


        dbFirebase.collection(DBFirebase.TABLE_DELIVERIES.name).document(delivery.getId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }


    public static void getDeliveries(final Context context, Boolean showLoading, final Response response){
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading,null);

        if(showLoading)
            myDialogLoading.dialog.show();


        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();

        dbFirebase.collection(DBFirebase.TABLE_DELIVERIES.name).get(Source.SERVER).addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                ArrayList<Delivery> deliveries = new ArrayList<>();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    Delivery delivery = doc.toObject(Delivery.class);
                    DocumentReference documentReference = (DocumentReference) doc.get("shoppingList");
                    if(documentReference != null)
                        delivery.setShoppingListId(documentReference.getId());
                    delivery.setId(doc.getId());
                    deliveries.add(delivery);
                }
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete(deliveries);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG, "Listen failed.", e);
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }


    public static void createShoppingList(final Context context, final ShoppingList shoppingList, Boolean showLoading, final Response response) {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading, null);

        if (showLoading)
            myDialogLoading.dialog.show();

        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();


        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put(DBFirebase.TABLE_SHOPPING_LISTS.column_uid, shoppingList.getUid());
        objectMap.put(DBFirebase.TABLE_SHOPPING_LISTS.column_itmes, shoppingList.getItems());
        objectMap.put(DBFirebase.TABLE_SHOPPING_LISTS.column_timeCreate, shoppingList.getTimeCreate());
        dbFirebase.collection(DBFirebase.TABLE_SHOPPING_LISTS.name).document(shoppingList.getId()).set(objectMap).addOnSuccessListener(new OnSuccessListener<Void>() {

            @Override
            public void onSuccess(Void aVoid) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }

    public static void getMyShoppingLists(final Context context, Boolean showLoading, final Response response){
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading,null);

        if(showLoading)
            myDialogLoading.dialog.show();


        final FirebaseFirestore dbFirebase = FirebaseFirestore.getInstance();

        dbFirebase.collection(DBFirebase.TABLE_SHOPPING_LISTS.name).get(Source.SERVER).addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                ArrayList<ShoppingList> deliveries = new ArrayList<>();
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    ShoppingList delivery = doc.toObject(ShoppingList.class);
                    delivery.setId(doc.getId());
                    deliveries.add(delivery);
                }
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onComplete(deliveries);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w(TAG, "Listen failed.", e);
                myDialogLoading.dialog.dismiss();
                if(response!=null){
                    response.onError(e);
                }
            }
        });
    }


    public static void changeStateDelivery(final Context context,Delivery delivery,int state, Boolean showLoading, final Response responseCallback)
    {
        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading,null);

        if(showLoading)
            myDialogLoading.dialog.show();

        Map<String, String> data = new HashMap<>();
        String call = "link";
            if(state == Delivery.State.requestSent) {
                call = "link";
                data.put("delivery", delivery.getId());
                data.put("shoppingList", delivery.getShoppingListId());
                data.put("requestererUid",delivery.getRequesterUid());
            }
            else if(state == Delivery.State.accepted) {
                call = "acceptDelivery";
                data.put("delivery", delivery.getId());
                data.put("shoppingList", delivery.getShoppingListId());
            }
            else if(state == Delivery.State.completed) {
                call = "deliveryDone";
                data.put("delivery", delivery.getId());
                data.put("shoppingList", delivery.getShoppingListId());
            }



        // Instantiate the RequestQueue.
        RequestQueue queue = SingletonRequestVolley.getInstance(context).getRequestQueue();
        String url = "https://us-central1-ditz-89dfd.cloudfunctions.net/" + call;

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        myDialogLoading.dialog.dismiss();
                        if(response!=null){
                            responseCallback.onComplete("");
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                myDialogLoading.dialog.dismiss();
                if(responseCallback!=null){
                    responseCallback.onError(error.getLocalizedMessage());
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                //params.put("Content-Type", "application/json");
                return params;
            }

            /*@Override
            public String getBodyContentType() {
                return "application/json";
            }*/

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return data;
            }

        };

// Add the request to the RequestQueue.
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);

    }


}
