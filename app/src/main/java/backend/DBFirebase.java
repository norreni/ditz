package backend;

import entities.Delivery;
import entities.User;

public class DBFirebase {

    public static class TABLE_USERS{
        public static final String name = "users";
        public static final String column_firstName = "firstName";
        public static final String column_lastName = "lastName";
        public static final String column_phone = "phone";
        public static final String column_address = "address";
        public static final String column_address_lat = "latitude";
        public static final String column_address_lng = "longitude";
        public static final String column_regisration_tokern = "registrationToken";
    }



    public static class TABLE_DELIVERIES{
        public static final String name = "deliveries";
        public static final String column_uidOfferer = "uidOfferer";
        public static final String column_uidApplicant = "uidApplicant";
        public static final String column_nameOfferer = "nameOfferer";
        public static final String column_surnameOfferer = "surnameOfferer";
        public static final String column_whereInfo = "whereInfo";
        public static final String column_timeCreate = "timeCreate";
        public static final String column_timeExecution = "timeExecution";
        public static final String column_limitPieces = "limitPieces";
        public static final String column_limitPrice = "limitPrice";
        public static final String column_type = "type";
        public static final String column_state = "state";
        public static final String column_lat = "latitude";
        public static final String column_lng = "longitude";
        public static final String column_address = "address";
    }


    public static class TABLE_SHOPPING_LISTS {
        public static final String name = "shoppingLists";
        public static final String column_uid = "uid";
        public static final String column_itmes = "items";
        public static final String column_timeCreate = "timeCreate";
    }

}


