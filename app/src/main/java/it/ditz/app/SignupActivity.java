package it.ditz.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.chaos.view.PinView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import backend.ApiRequest;
import entities.User;
import it.MyApplication;
import tools.IntentsParameters;
import tools.MyDialog;

public class SignupActivity extends AppCompatActivity {

    EditText editName,editSurname,editPhone,editAddress;
    LatLng latLng;
    String address;
    Context context = this;
    String verificationId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        setContentView(R.layout.activity_signup);



        editName = findViewById(R.id.editName);
        editSurname = findViewById(R.id.editSurname);
        editAddress = findViewById(R.id.editAddress);
        editPhone = findViewById(R.id.editPhone);

        editPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b && editPhone.getText().toString().isEmpty())
                    editPhone.setText("+39");
            }
        });

        editAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    onClickChooseAddress(editAddress);
            }
        });
    }

    public void onClickLeftButton(View view){
        finish();
    }


    MyDialog dialogLocalOtp;
    public void onClickSignup(View view){
        final String phone = editPhone.getText().toString().trim();
        final String name = editName.getText().toString();
        final String surname = editSurname.getText().toString();
        final String address = editAddress.getText().toString();
        if(phone.isEmpty() || name.isEmpty() || surname.isEmpty() || address.isEmpty())
        {
            Toast.makeText(this,R.string.error_message_empty_field,Toast.LENGTH_SHORT).show();
            return;
        }

        if(!phone.contains("+"))
        {
            Toast.makeText(this,R.string.error_message_no_prefix,Toast.LENGTH_SHORT).show();
            return;
        }

        final MyDialog myDialogLoading = new MyDialog(context, R.layout.dialog_loading,null);

        myDialogLoading.show();
        ApiRequest.phoneExist(context, phone, false, new ApiRequest.Response() {
            @Override
            public void onComplete(Object value) {
                boolean exist = (boolean) value;
                if(exist){
                    myDialogLoading.dismiss();
                    Toast.makeText(context,R.string.error_message_phone_exist,Toast.LENGTH_LONG).show();
                }
                else {
                    ApiRequest.sendOtp(context,phone, false, new ApiRequest.Response() {
                        @Override
                        public void onComplete(Object value) {
                            myDialogLoading.dialog.dismiss();
                            if(value instanceof String){
                                verificationId = (String) value;
                                dialogLocalOtp.show();                }
                            else {
                                //significa che il numero è stato già verificato, senza mandare l'top
                                //login();
                                //ApiRequest signup
                                User user = new User();
                                user.setFirstName(name);
                                user.setLastName(surname);
                                user.setPhone(phone);
                                user.setAddress(address);
                                user.setLatitude(latLng.latitude);
                                user.setLongitude(latLng.longitude);
                                user.setRegistrationToken(MyApplication.registrationToken);
                                user.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                ApiRequest.signup(context, user, false, new ApiRequest.Response() {
                                    @Override
                                    public void onComplete(Object value) {
                                        myDialogLoading.dismiss();
                                        Intent intent = new Intent(context,MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }

                                    @Override
                                    public void onError(Object value) {
                                        myDialogLoading.dismiss();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onError(Object value) {
                            myDialogLoading.dialog.dismiss();
                            if(value instanceof FirebaseException){
                                FirebaseException error = (FirebaseException) value;
                                Toast.makeText(context,error.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }

            @Override
            public void onError(Object value) {
                myDialogLoading.dismiss();
            }
        });



        final MyDialog.MyDialogOption option = new MyDialog.MyDialogOption();
        option.setResButtonCancel(R.id.dialogViewActionCancel);
        option.setResButtonConfirm(R.id.dialogViewActionConfirm);
        option.setResCodeOtp(R.id.dialoCodeOtp);
        option.setResOtpTextView(R.id.dialogPinView);
        option.setOnClickListenerCancel(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocalOtp.dismiss();
            }
        });
        option.setOnClickListenerConfirm(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PinView pinView = dialogLocalOtp.dialog.findViewById(option.getResOtpTextView());
                String pin = pinView.getText().toString();
                if(pin.length() == 6){
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, pin);
                    myDialogLoading.dialog.show();
                    FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            dialogLocalOtp.dismiss();
                            myDialogLoading.dialog.dismiss();
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                //ApiRequest signup
                                User user = new User();
                                user.setFirstName(name);
                                user.setLastName(surname);
                                user.setPhone(phone);
                                user.setAddress(address);
                                user.setLatitude(latLng.latitude);
                                user.setLongitude(latLng.longitude);
                                user.setRegistrationToken(MyApplication.registrationToken);
                                user.setUid(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                ApiRequest.signup(context, user, false, new ApiRequest.Response() {
                                    @Override
                                    public void onComplete(Object value) {
                                        myDialogLoading.dismiss();
                                        Intent intent = new Intent(context,MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }

                                    @Override
                                    public void onError(Object value) {
                                        myDialogLoading.dismiss();
                                    }
                                });

                                // ...
                            } else {
                                myDialogLoading.dismiss();
                                // Sign in failed, display a message and update the UI
                                Log.w("OtpVerification", "signInWithCredential:failure", task.getException());
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    // The verification code entered was invalid
                                    FirebaseAuthInvalidCredentialsException exception = (FirebaseAuthInvalidCredentialsException) task.getException();
                                    Toast.makeText(context,exception.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                }
            }
        });

        dialogLocalOtp = new MyDialog(this,R.layout.dialog_otp,option);
        dialogLocalOtp.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

    }

    public void onClickChooseAddress(View view){
        if(checkPermission()){
            startActivityForResult(new Intent(this,ChooseAddressActivity.class),IntentsParameters.Request.REQUEST_CHOOSE_ADDRESS_ON_CAP);
        }
    }

    public boolean checkPermission(){

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    IntentsParameters.Request.REQUEST_COARSE_LOCATION);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){
            startActivityForResult(new Intent(this,ChooseAddressActivity.class),IntentsParameters.Request.REQUEST_CHOOSE_ADDRESS_ON_CAP);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IntentsParameters.Request.REQUEST_CHOOSE_ADDRESS_ON_CAP){
            if(resultCode == RESULT_OK){

                double lat = data.getDoubleExtra(IntentsParameters.Extra.EXTRA_KEY_ADDRESS_LATITUDE,0);
                double lng = data.getDoubleExtra(IntentsParameters.Extra.EXTRA_KEY_ADDRESS_LONGITUDE,0);
                address = data.getStringExtra(IntentsParameters.Extra.EXTRA_KEY_ADDRESS_NAME);
                latLng = new LatLng(lat,lng);
                editAddress.setText(address);
            }
        }
    }
}
