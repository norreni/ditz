package it.ditz.app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.protobuf.Api;
import com.potyvideo.library.AndExoPlayerView;
import com.potyvideo.library.utils.PathUtil;

import java.net.URISyntaxException;

import backend.ApiRequest;
import entities.SessionManager;
import entities.User;
import it.MyApplication;
import tools.MyDialog;
import tools.Utils;

public class LoginActivity extends AppCompatActivity {

    EditText editPhone;
    Context context = this;
    String verificationId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        editPhone = findViewById(R.id.editPhone);

        editPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b && editPhone.getText().toString().isEmpty())
                    editPhone.setText("+39");
            }
        });

        if (FirebaseAuth.getInstance().getCurrentUser()!= null){
            User user = User.load();
            if(user != null && user.getPhone().equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())){
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {

                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();
                                user.setRegistrationToken(token);
                                if(Utils.isNetworkAvailable())
                                    //per salvare il registration token
                                    ApiRequest.signup(context,user,false,null);
                            }
                        });

                SessionManager.getInstance(context).currentUser = user;
                startActivity(new Intent(this,MainActivity.class));
                finish();
                return;
            }
        }

        AndExoPlayerView andExoPlayerView = findViewById(R.id.andExoPlayerView);
        Uri path = Uri.parse("file:///android_asset/video.mp4");

       /* try {
            String filePath = PathUtil.getPath(this, path);
            andExoPlayerView.setSource(filePath);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }*/


    }

    public void onClickSignup(View view){
        startActivity(new Intent(this,SignupActivity.class));
    }


    MyDialog dialogLocalOtp;
    public void onClickLogin(View view){
        final String phone = editPhone.getText().toString();
        if(phone.isEmpty())
        {
            Toast.makeText(this,R.string.error_message_empty_field,Toast.LENGTH_SHORT).show();
            return;
        }

        if(!phone.contains("+"))
        {
            Toast.makeText(this,R.string.error_message_no_prefix,Toast.LENGTH_SHORT).show();
            return;
        }

        if(!Utils.isNetworkAvailable())
        {
            Toast.makeText(this,R.string.error_message_no_network,Toast.LENGTH_SHORT).show();
            return;
        }

        final MyDialog dialogLoading = new MyDialog(context);
        dialogLoading.show();
        ApiRequest.phoneExist(this, phone, false, new ApiRequest.Response() {
            @Override
            public void onComplete(Object value) {
                boolean exist = (boolean) value;
                if(exist){
                    ApiRequest.sendOtp(context,phone, false, new ApiRequest.Response() {
                        @Override
                        public void onComplete(Object value) {
                            dialogLoading.dismiss();
                            if(value instanceof String){
                                verificationId = (String) value;
                                dialogLocalOtp.show();
                            }
                            else {
                                //significa che il numero è stato già verificato, senza mandare l'top
                                ApiRequest.login(context, phone, false, new ApiRequest.Response() {
                                    @Override
                                    public void onComplete(Object value) {
                                        dialogLoading.dismiss();
                                        startActivity(new Intent(context,MainActivity.class));
                                        finish();
                                    }

                                    @Override
                                    public void onError(Object value) {
                                        dialogLoading.dismiss();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onError(Object value) {
                            dialogLoading.dismiss();
                            if(value instanceof FirebaseException){
                                FirebaseException error = (FirebaseException) value;
                                Toast.makeText(context,error.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else {
                    dialogLoading.dismiss();
                    Toast.makeText(context,R.string.error_message_phone_no_exist,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Object value) {

            }
        });

        final MyDialog.MyDialogOption option = new MyDialog.MyDialogOption();
        option.setResButtonCancel(R.id.dialogViewActionCancel);
        option.setResButtonConfirm(R.id.dialogViewActionConfirm);
        option.setResCodeOtp(R.id.dialoCodeOtp);
        option.setResOtpTextView(R.id.dialogPinView);
        option.setOnClickListenerCancel(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLocalOtp.dismiss();
            }
        });
        option.setOnClickListenerConfirm(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PinView pinView = dialogLocalOtp.dialog.findViewById(option.getResOtpTextView());
                String pin = pinView.getText().toString();
                if(pin.length() == 6){
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, pin);
                    dialogLoading.dialog.show();
                    FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            dialogLocalOtp.dismiss();
                            dialogLoading.dismiss();
                            if (task.isSuccessful()) {
                                //significa che il numero è stato già verificato, senza mandare l'top
                                ApiRequest.login(context, phone, false, new ApiRequest.Response() {
                                    @Override
                                    public void onComplete(Object value) {
                                        dialogLoading.dismiss();
                                        startActivity(new Intent(context,MainActivity.class));
                                        finish();
                                    }

                                    @Override
                                    public void onError(Object value) {
                                        dialogLoading.dismiss();
                                    }
                                });

                                // ...
                            } else {
                                dialogLoading.dismiss();
                                // Sign in failed, display a message and update the UI
                                Log.w("OtpVerification", "signInWithCredential:failure", task.getException());
                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    // The verification code entered was invalid
                                    FirebaseAuthInvalidCredentialsException exception = (FirebaseAuthInvalidCredentialsException) task.getException();
                                    Toast.makeText(context,exception.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                }
            }
        });

        dialogLocalOtp = new MyDialog(this,R.layout.dialog_otp,option);
        dialogLocalOtp.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

    }
}
