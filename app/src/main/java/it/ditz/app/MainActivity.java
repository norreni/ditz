package it.ditz.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.chinodev.androidneomorphframelayout.NeomorphFrameLayout;
import com.skyfishjy.library.RippleBackground;


import entities.SessionManager;
import entities.User;
import fragment.DeliveriesFragment;
import fragment.MyMapFragment;

import fragment.ShoppingListsFragment;
import fragment.dialog.CreateDeliveryDialogFragment;
import fragment.dialog.CreateShoppingListDialogFragment;
import neomorphism.MyNeomorphFrameLayout;
import tools.IntentsParameters;
import tools.MyDialog;

public class MainActivity extends FragmentActivity {


    Context context = this;

    TextView txtName,txtAdrress,txtInfo;

    MyDialog dialogMenu;
    ViewFlipper viewFlipper;
    MyNeomorphFrameLayout buttonMap,buttonCreateDelivery,buttonShoppingLists,buttonMyDeliveries,lastButtonFocus;
    public MyMapFragment mapFragment;
    public DeliveriesFragment deliveriesFragment;
    public ShoppingListsFragment shoppingListsFragment;
    enum FragmentsType{
        map,
        deliveries,
        shoppingLists
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Window window = getWindow();
        WindowManager.LayoutParams winParams = window.getAttributes();
        winParams.flags &= ~WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        window.setAttributes(winParams);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);


        txtName = findViewById(R.id.txtName);
        txtInfo = findViewById(R.id.txtInfo);
        txtAdrress = findViewById(R.id.txtAddress);
        viewFlipper = findViewById(R.id.viewFlipper);
        buttonMap = findViewById(R.id.buttonMap);
        buttonCreateDelivery = findViewById(R.id.buttonCreateDelivery);
        buttonShoppingLists = findViewById(R.id.buttonShoppingLists);
        lastButtonFocus = buttonMap;

        User user =  SessionManager.getInstance(context).currentUser;
        txtName.setText(user.getFirstName()+" "+user.getLastName());
        txtAdrress.setText(user.getAddress());

        dialogMenu = new MyDialog(this,R.layout.dialog_menu,null);

        dialogMenu.dialog.findViewById(R.id.dialogClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogMenu.dismiss();
            }
        });
        dialogMenu.dialog.findViewById(R.id.dialogLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SessionManager.getInstance(context).logout();
                startActivity(new Intent(context,LoginActivity.class));
                finish();
            }
        });


        dialogMenu.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        mapFragment = (MyMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        deliveriesFragment = (DeliveriesFragment) getSupportFragmentManager().findFragmentById(R.id.deliveriesFragment);
        shoppingListsFragment = (ShoppingListsFragment) getSupportFragmentManager().findFragmentById(R.id.shoppingListsFragment);

        viewFlipper.setInAnimation(this, R.anim.slide_in_right);
        viewFlipper.setOutAnimation(this, R.anim.slide_out_right);
        showFragment(FragmentsType.map);
    }




    public void onClickMenu(View view){
        dialogMenu.show();
    }

    public void onClickCrateDelivery(View view){
        CreateDeliveryDialogFragment createDeliveryDialogFragment = new CreateDeliveryDialogFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(createDeliveryDialogFragment.getClass().getName());
        if (prev != null) {
            return;
        }
        fragmentTransaction.addToBackStack(null);
        createDeliveryDialogFragment.show(fragmentTransaction, createDeliveryDialogFragment.getClass().getName());

    }

    public void onClickCrateShoppingList(View view){
        CreateShoppingListDialogFragment fragment = new CreateShoppingListDialogFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(fragment.getClass().getName());
        if (prev != null) {
            return;
        }
        fragmentTransaction.addToBackStack(null);
        fragment.show(fragmentTransaction, fragment.getClass().getName());

    }

    public void onClickDeliveries(View view){
        if(viewFlipper.getDisplayedChild() == 1)
            return;
        showFragment(FragmentsType.deliveries);
    }

    public void onClickMap(View view){
        if(viewFlipper.getDisplayedChild() == 0)
            return;
        showFragment(FragmentsType.map);
    }

    public void onClickShoppinList(View view){
        if(viewFlipper.getDisplayedChild() == 2)
            return;
        showFragment(FragmentsType.shoppingLists);
    }


    public void showFragment(FragmentsType type){
        lastButtonFocus.setShadowOuter();


        if(type == FragmentsType.map){
            buttonMap.setShadowInner();
            lastButtonFocus = buttonMap;
            viewFlipper.setDisplayedChild(0);
            txtInfo.setText(R.string.info_sez_map);
        }
        else if(type == FragmentsType.deliveries){
            buttonCreateDelivery.setShadowInner();
            lastButtonFocus = buttonCreateDelivery;
            viewFlipper.setDisplayedChild(1);
            deliveriesFragment.onResume();
            txtInfo.setText(R.string.info_sez_deliveries);
        }
        else if(type == FragmentsType.shoppingLists){
            buttonShoppingLists.setShadowInner();
            lastButtonFocus = buttonShoppingLists;
            viewFlipper.setDisplayedChild(2);
            shoppingListsFragment.onResume();
            txtInfo.setText(R.string.info_sez_list);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        deliveriesFragment.onResume();
    }

    public void reloadData(){
        deliveriesFragment.onResume();
        shoppingListsFragment.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(IntentsParameters.Request.REQUEST_NOTIFICATION_RECIVER)
        );
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                mapFragment.reloadDeliveries();
                deliveriesFragment.onResume();
                shoppingListsFragment.onResume();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void onClickShowDeliveryPending(View view){
        User user = SessionManager.getInstance(this).currentUser;
        if(user.getDeliveryInProgress()!=null){
            mapFragment.showDetailDelivery(user.getDeliveryInProgress());
        }
    }
    public void changeStateDeliveryPending(){
        MyNeomorphFrameLayout imageDeliveryPending = findViewById(R.id.imageDeliveryPending);
        RippleBackground ripple = findViewById(R.id.ripple);
        User user = SessionManager.getInstance(this).currentUser;
        if(user.getDeliveryInProgress()!=null){
            imageDeliveryPending.setVisibility(View.VISIBLE);
            ripple.startRippleAnimation();
        }
        else{
            imageDeliveryPending.setVisibility(View.INVISIBLE);
            ripple.stopRippleAnimation();
        }

    }
}
