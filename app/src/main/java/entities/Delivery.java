package entities;

import android.net.ParseException;

import com.google.firebase.firestore.DocumentReference;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Delivery implements Serializable,Cloneable {

    public static class State{
        public static int none = 0;
        public static int requestSent = 1;
        public static int accepted = 2;
        public static int completed = 3;
    }

    public static class Type{
        public static int market = 0;
        public static int Pharmacy = 1;
    }

    private String id = "";
    private String uidOfferer= "";
    private String requesterUid = "";
    private String whereInfo = "";
    private User userOfferer;
    private User userApplicant;
    private long timeCreate;
    private long timeExecution;
    private int limitPieces = 20;
    private double limitPrice = 20;
    private int type = Type.market;
    private int state = State.none;
    private double latitude;
    private double longitude;
    private String address = "";
    private String shoppingListItems = "";
    private String shoppingListId = "";

    String nameOfferer = "";
    String surnameOfferer = "";
    String requesterName = "";
    String requesterSurname = "";


    public Delivery(){}

    public void setId(String id) {
        this.id = id;
    }

    public void setLimitPieces(int limitPieces) {
        this.limitPieces = limitPieces;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public void setLimitPrice(double limitPrice) {
        this.limitPrice = limitPrice;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setTimeExecution(long timeExecution) {
        this.timeExecution = timeExecution;
    }

    public void setType(int type) {
        this.type = type;
    }




    public String getId() {
        return id;
    }

    public double getLimitPrice() {
        return limitPrice;
    }

    public int getLimitPieces() {
        return limitPieces;
    }

    public int getType() {
        return type;
    }

    public int getState() {
        return state;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public long getTimeExecution() {
        return timeExecution;
    }


    public void setRequesterUid(String uidApplicant) {
        this.requesterUid = uidApplicant;
    }

    public void setUidOfferer(String uidOfferer) {
        this.uidOfferer = uidOfferer;
    }

    public void setUserApplicant(User userApplicant) {
        this.userApplicant = userApplicant;
    }

    public void setUserOfferer(User userOfferer) {
        this.userOfferer = userOfferer;
    }

    public String getRequesterUid() {
        return requesterUid;
    }

    public String getUidOfferer() {
        return uidOfferer;
    }

    public User getUserApplicant() {
        return userApplicant;
    }

    public User getUserOfferer() {
        return userOfferer;
    }

    public void setWhereInfo(String whereInfo) {
        this.whereInfo = whereInfo;
    }

    public String getWhereInfo() {
        return whereInfo;
    }

    public String getTimeExecute(String forrmat){
        SimpleDateFormat formatter = new SimpleDateFormat(forrmat);
        try
        {

            Date date = new Date(timeExecution);
            return formatter.format(date);

        } catch (ParseException e)
        {
            e.printStackTrace();
        }
        return "";
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setNameOfferer(String nameOfferer) {
        this.nameOfferer = nameOfferer;
    }

    public void setSurnameOfferer(String surnameOfferer) {
        this.surnameOfferer = surnameOfferer;
    }

    public String getNameOfferer() {
        return nameOfferer;
    }

    public String getSurnameOfferer() {
        return surnameOfferer;
    }

    public void setRequesterName(String nameRequester) {
        this.requesterName = nameRequester;
    }

    public void setRequesterSurname(String surnameRequester) {
        this.requesterSurname = surnameRequester;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public String getRequesterSurname() {
        return requesterSurname;
    }

    public void setShoppingListItems(String shoppingListItems) {
        this.shoppingListItems = shoppingListItems;
    }

    public String getShoppingListItems() {
        return shoppingListItems;
    }

    public void setShoppingListId(String shoppingListId) {
        this.shoppingListId = shoppingListId;
    }

    public String getShoppingListId() {
        return shoppingListId;
    }
}
