package entities;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import tools.Utils;

public class SessionManager {

    private Context mContext;
    private static SessionManager mSessionManager = null;
    private static Activity mActivity;
    public User currentUser;


    public static SessionManager getInstance(Context context){
        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
        else mActivity = null;

        if(mSessionManager == null){
            mSessionManager = new SessionManager(context);
        }
        return mSessionManager;
    }

    private SessionManager(Context context){
        mContext = context;
    }

    public void logout(){
        FirebaseAuth.getInstance().signOut();
        Utils.removeObject(mContext,User.class);
    }
}
