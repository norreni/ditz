package entities;

import java.io.Serializable;
import java.util.ArrayList;

import it.MyApplication;
import tools.Utils;

public class User implements Serializable,Cloneable {

    private String uid = "";
    private String firstName = "";
    private String lastName = "";
    private String phone = "";
    private String address = "";
    private String registrationToken = "";
    private double latitude;
    private double longitude;

    public ArrayList<Delivery> myDelieveris;
    public ArrayList<Delivery> allDelieveris;
    public ArrayList<ShoppingList> myShoppingLists;

    public User(){
        myDelieveris = new ArrayList<>();
        myShoppingLists = new ArrayList<>();
        allDelieveris = new ArrayList<>();
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getUid() {
        return uid;
    }

    public static User load(){
        User user = Utils.getObjectFromPrefs(MyApplication.getContext(),User.class);
        return user;
    }

    public void saveInLocal(){
        Utils.storeObject(MyApplication.getContext(),this);
    }

    public void setMyDelieveris(ArrayList<Delivery> delieveris){
        this.myDelieveris.clear();
        this.allDelieveris.clear();
        this.allDelieveris.addAll(delieveris);
        for(Delivery delivery : delieveris){
            if(delivery.getUidOfferer().endsWith(this.uid))
                this.myDelieveris.add(delivery);
        }
    }

    public Delivery getDeliveryInProgress(){
        Delivery delivery = null;
        for(Delivery d : allDelieveris){
            if(d.getState() != Delivery.State.none && ((d.getUidOfferer().equals(this.uid) && d.getState()!= Delivery.State.completed) || d.getRequesterUid().equals(this.uid))){
                delivery = d;
                break;
            }
        }

        return delivery;
    }

    public void setMyShoppingLists(ArrayList<ShoppingList> myShoppingLists) {
        this.myShoppingLists.clear();
        this.myShoppingLists.addAll(myShoppingLists);
    }

    public ArrayList<Delivery> getMyDelieveris() {
        return myDelieveris;
    }

    public ArrayList<ShoppingList> getMyShoppingLists() {
        return myShoppingLists;
    }

    public void setRegistrationToken(String registrationToken) {
        this.registrationToken = registrationToken;
    }

    public String getRegistrationToken() {
        return registrationToken;
    }
}
