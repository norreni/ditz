package entities;

import java.io.Serializable;

public class ShoppingList implements Serializable,Cloneable {

    private String id = "";
    private String uid = "";
    private String items = "";
    private long timeCreate;
    public ShoppingList(){

    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getUid() {
        return uid;
    }

    public String getId() {
        return id;
    }

    public String getItems() {
        return items;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public long getTimeCreate() {
        return timeCreate;
    }
}
