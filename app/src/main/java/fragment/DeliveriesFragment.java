package fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import adapter.DeliveriesAdapter;
import entities.Delivery;
import entities.SessionManager;
import it.ditz.app.R;

public class DeliveriesFragment extends Fragment {

    View rootView;
    RecyclerView recyclerView;
    DeliveriesAdapter adapterMyDeliveries;
    ArrayList<Delivery> myDeliveries = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_deliveries, container, false);

        recyclerView = rootView.findViewById(R.id.recycleView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapterMyDeliveries = new DeliveriesAdapter(getContext(),myDeliveries);
        recyclerView.setAdapter(adapterMyDeliveries);

        reload();

        return rootView;
    }


    public void reload(){
        myDeliveries.clear();

        myDeliveries.addAll(SessionManager.getInstance(getContext()).currentUser.getMyDelieveris());
        Collections.sort(myDeliveries, new Comparator<Delivery>() {
            @Override
            public int compare(Delivery lhs, Delivery rhs) {
                return Long.compare(lhs.getTimeExecution(), rhs.getTimeExecution());
            }
        });
        adapterMyDeliveries.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        reload();
    }
}