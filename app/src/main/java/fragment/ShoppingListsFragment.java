package fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.security.interfaces.RSAKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import adapter.DeliveriesAdapter;
import adapter.ShoppingListsAdapter;
import backend.ApiRequest;
import entities.Delivery;
import entities.SessionManager;
import entities.ShoppingList;
import it.ditz.app.R;
import tools.Utils;

public class ShoppingListsFragment extends Fragment {

    View rootView;
    RecyclerView recyclerView;
    ShoppingListsAdapter adapterMyDeliveries;
    ArrayList<ShoppingList> shoppingLists = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_shopping_lists, container, false);

        recyclerView = rootView.findViewById(R.id.recycleView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapterMyDeliveries = new ShoppingListsAdapter(getContext(),shoppingLists,false);
        recyclerView.setAdapter(adapterMyDeliveries);

        reload();

        return rootView;
    }


    public void reload(){

        shoppingLists.clear();
        shoppingLists.addAll(SessionManager.getInstance(getContext()).currentUser.getMyShoppingLists());
        Collections.sort(shoppingLists, new Comparator<ShoppingList>() {
            @Override
            public int compare(ShoppingList lhs, ShoppingList rhs) {
                return Long.compare(rhs.getTimeCreate(), lhs.getTimeCreate());
            }
        });
        adapterMyDeliveries.notifyDataSetChanged();


        if(Utils.isNetworkAvailable()){
            ApiRequest.getMyShoppingLists(getContext(), false, new ApiRequest.Response() {
                @Override
                public void onComplete(Object value) {
                    shoppingLists.clear();
                    ArrayList<ShoppingList> items = (ArrayList<ShoppingList>) value;
                    shoppingLists.addAll(items);
                    SessionManager.getInstance(getContext()).currentUser.setMyShoppingLists(items);
                    Collections.sort(shoppingLists, new Comparator<ShoppingList>() {
                        @Override
                        public int compare(ShoppingList lhs, ShoppingList rhs) {
                            return Long.compare(rhs.getTimeCreate(), lhs.getTimeCreate());
                        }
                    });
                    adapterMyDeliveries.notifyDataSetChanged();
                }

                @Override
                public void onError(Object value) {

                }
            });
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        reload();
    }
}