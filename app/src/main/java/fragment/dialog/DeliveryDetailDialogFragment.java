package fragment.dialog;

import android.app.Dialog;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import adapter.ShoppingListsAdapter;
import backend.ApiRequest;
import entities.Delivery;
import entities.SessionManager;
import entities.ShoppingList;
import entities.User;
import it.ditz.app.MainActivity;
import it.ditz.app.R;
import neomorphism.MyNeomorphFrameLayout;
import tools.MyDialog;
import tools.Utils;

public class DeliveryDetailDialogFragment extends DialogFragment {

    MyDialog myDialog;
    public Delivery delivery;

    RecyclerView recyclerView;
    ShoppingListsAdapter adapterShoppingList;
    ArrayList<ShoppingList> shoppingLists = new ArrayList<>();

    ViewFlipper viewFlipper;
    public DeliveryDetailDialogFragment(Delivery delivery){
        this.delivery = delivery;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        myDialog = new MyDialog(getContext(), R.layout.fragment_delivery_detail,null);

        viewFlipper = myDialog.dialog.findViewById(R.id.viewFlipper);
        myDialog.dialog.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeliveryDetailDialogFragment.this.dismiss();
            }
        });



        myDialog.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        recyclerView = myDialog.dialog.findViewById(R.id.recycleView);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        shoppingLists.addAll(SessionManager.getInstance(getContext()).currentUser.getMyShoppingLists());
        adapterShoppingList = new ShoppingListsAdapter(getContext(),shoppingLists,true);
        recyclerView.setAdapter(adapterShoppingList);

        reloadRecap();
        return myDialog.dialog;
    }

    public void reloadRecap(){
        TextView txtTitle = myDialog.dialog.findViewById(R.id.txtTitle);
        TextView txtWhere = myDialog.dialog.findViewById(R.id.txtWhere);
        TextView txtWhen = myDialog.dialog.findViewById(R.id.txtWhen);
        TextView txtRange = myDialog.dialog.findViewById(R.id.txtRange);

        AppCompatImageView imageCategory = myDialog.dialog.findViewById(R.id.icon_category);
        AppCompatImageView imageWhere = myDialog.dialog.findViewById(R.id.icon_where);
        AppCompatImageView imageWhen = myDialog.dialog.findViewById(R.id.icon_when);
        AppCompatImageView imageRange = myDialog.dialog.findViewById(R.id.icon_range);

        int color = delivery.getType() == Delivery.Type.market ? getResources().getColor(R.color.colorFood) : getResources().getColor(R.color.colorPharma);

        imageCategory.setImageResource(delivery.getType() == Delivery.Type.market ? R.drawable.ic_restaurant_menu_black_24dp : R.drawable.ic_local_hospital_black_24dp);
        imageCategory.setColorFilter(color);
        imageWhere.setColorFilter(color);
        imageWhen.setColorFilter(color);
        imageRange.setColorFilter(color);

        final User user = SessionManager.getInstance(getContext()).currentUser;

        txtWhere.setText(delivery.getWhereInfo());
        txtWhen.setText(delivery.getTimeExecute("E MMMM dd, hh:mm a"));
        txtRange.setText("Max "+delivery.getLimitPieces()+"pz  Max "+delivery.getLimitPrice()+"€");
        adapterShoppingList.notifyDataSetChanged();

        if(delivery.getState() == Delivery.State.none) {
            if(delivery.getUidOfferer().equals(user.getUid()))
                txtTitle.setText(delivery.getType() == Delivery.Type.market ? "Sei disponibile per una\nconsegna di generi alimentari" : "Sei disponibile per una\nconsegna di prodotti farmaceutici");
            else {
                String name = delivery.getNameOfferer()+" "+delivery.getSurnameOfferer();
                txtTitle.setText(delivery.getType() == Delivery.Type.market ? name+" è disponibile per una\nconsegna di generi alimentari" : name+" è disponibile per una\nconsegna di prodotti farmaceutici");

            }

            Location locationA  = new Location("a");
            locationA.setLatitude(delivery.getLatitude());
            locationA.setLongitude(delivery.getLongitude());

            Location locationB  = new Location("b");
            locationB.setLatitude(user.getLatitude());
            locationB.setLongitude(user.getLongitude());
            if(locationA.distanceTo(locationB)>100){
                viewFlipper.setDisplayedChild(2);
                myDialog.dialog.findViewById(R.id.buttonRichiedi).setVisibility(View.GONE);
                return;
            }

            viewFlipper.setDisplayedChild(0);


            myDialog.dialog.findViewById(R.id.buttonRichiedi).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(adapterShoppingList.getItemCount() == 0)
                    {
                        Toast.makeText(getContext(),"Crea prima una lista della sepsa nella sezione dedicata",Toast.LENGTH_LONG).show();
                        return;
                    }
                    if(adapterShoppingList.selectedShoppingList == null)
                    {
                        Toast.makeText(getContext(),"seleziona una lista della sepsa",Toast.LENGTH_LONG).show();
                        return;
                    }
                    delivery.setRequesterUid(user.getUid());
                    delivery.setShoppingListId(adapterShoppingList.selectedShoppingList.getId());
                    ApiRequest.changeStateDelivery(getContext(),delivery,Delivery.State.requestSent,true, new ApiRequest.Response() {
                        @Override
                        public void onComplete(Object value) {
                            DeliveryDetailDialogFragment.this.dismiss();

                        }

                        @Override
                        public void onError(Object value) {
                            Toast.makeText(getContext(),R.string.error_message_general,Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
        else if(delivery.getState() == Delivery.State.requestSent) {
            viewFlipper.setDisplayedChild(3);

            TextView txtTitleShop = myDialog.dialog.findViewById(R.id.txtTitleShop);
            TextView txtInfoShop = myDialog.dialog.findViewById(R.id.txtSopINfo);
            if(delivery.getUidOfferer().equals(user.getUid())) {
                MyNeomorphFrameLayout button = myDialog.dialog.findViewById(R.id.buttonRichiedi);
                ((TextView)button.getChildAt(0)).setText("Conferma richiesta");
                txtTitle.setText(delivery.getType() == Delivery.Type.market ? "Sei disponibile per una\nconsegna di generi alimentari" : "Sei disponibile per una\nconsegna di prodotti farmaceutici");
                txtTitleShop.setText(delivery.getRequesterName()+" "+delivery.getRequesterSurname()+" \nrichiede i seguenti articoli:");
                txtInfoShop.setText(delivery.getShoppingListItems());
                myDialog.dialog.findViewById(R.id.buttonRichiedi).setOnClickListener(new View.OnClickListener() {
                    @Override
                     public void onClick(View view) {
                        ApiRequest.changeStateDelivery(getContext(),delivery,Delivery.State.accepted,true, new ApiRequest.Response() {
                            @Override
                            public void onComplete(Object value) {
                                delivery.setState(Delivery.State.accepted);
                                DeliveryDetailDialogFragment.this.dismiss();

                            }

                            @Override
                            public void onError(Object value) {
                                Toast.makeText(getContext(),R.string.error_message_general,Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

            }
            else {
                myDialog.dialog.findViewById(R.id.buttonRichiedi).setVisibility(View.GONE);
                String name = delivery.getNameOfferer()+" "+delivery.getSurnameOfferer();
                txtTitle.setText(delivery.getType() == Delivery.Type.market ? name+" è disponibile per una\nconsegna di generi alimentari" : name+" è disponibile per una\nconsegna di prodotti farmaceutici");
                txtTitleShop.setText("Hai richiesto i seguenti articoli:");
                txtInfoShop.setText(delivery.getShoppingListItems());
            }
        }
        else if(delivery.getState() == Delivery.State.accepted) {
            viewFlipper.setDisplayedChild(3);

            TextView txtTitleShop = myDialog.dialog.findViewById(R.id.txtTitleShop);
            TextView txtInfoShop = myDialog.dialog.findViewById(R.id.txtSopINfo);
            if(delivery.getUidOfferer().equals(user.getUid())) {
                MyNeomorphFrameLayout button = myDialog.dialog.findViewById(R.id.buttonRichiedi);
                ((TextView)button.getChildAt(0)).setText("Consegnata");
                txtTitle.setText(delivery.getType() == Delivery.Type.market ? "Consegnerai generi alimentari" : "Consegnerai prodotti farmaceutici");
                txtTitleShop.setText(delivery.getRequesterName()+" "+delivery.getRequesterSurname()+" \nrichiede i seguenti articoli:");
                txtInfoShop.setText(delivery.getShoppingListItems());
                myDialog.dialog.findViewById(R.id.buttonRichiedi).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ApiRequest.changeStateDelivery(getContext(),delivery,Delivery.State.completed,true, new ApiRequest.Response() {
                            @Override
                            public void onComplete(Object value) {
                                delivery.setState(Delivery.State.completed);
                                DeliveryDetailDialogFragment.this.dismiss();

                            }

                            @Override
                            public void onError(Object value) {
                                Toast.makeText(getContext(),R.string.error_message_general,Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
            else {
                myDialog.dialog.findViewById(R.id.buttonRichiedi).setVisibility(View.GONE);
                String name = delivery.getNameOfferer()+" "+delivery.getSurnameOfferer();
                txtTitle.setText(delivery.getType() == Delivery.Type.market ? name+" ti consegnerà\ngeneri alimentari" : name+" ti consegnerà\n prodotti farmaceutici");
                txtTitleShop.setText("Hai richiesto i seguenti articoli:");
                txtInfoShop.setText(delivery.getShoppingListItems());
            }
        }
        else if(delivery.getState() == Delivery.State.completed) {
            View view = myDialog.dialog.findViewById(R.id.viewPay);
            view.setVisibility(View.VISIBLE);
            TextView txtInfoPay = view.findViewById(R.id.txtPay);

            txtInfoPay.setText(delivery.getNameOfferer()+" "+delivery.getSurnameOfferer()+"\n ha consegnato la spesa, completa il pagamento:");
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Utils.isNetworkAvailable()){
                        ApiRequest.deleteDelivery(getContext(), delivery, true, new ApiRequest.Response() {
                            @Override
                            public void onComplete(Object value) {
                                ((MainActivity)getActivity()).mapFragment.reloadDeliveries();
                                ((MainActivity)getActivity()).reloadData();
                                DeliveryDetailDialogFragment.this.dismiss();
                            }

                            @Override
                            public void onError(Object value) {

                            }
                        });
                    }
                }
            };

            view.findViewById(R.id.buttonChiudi).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DeliveryDetailDialogFragment.this.dismiss();
                }
            });

            view.findViewById(R.id.buttonContanti).setOnClickListener(onClickListener);
            view.findViewById(R.id.buttonMancia).setOnClickListener(onClickListener);
            view.findViewById(R.id.buttonPaypal).setOnClickListener(onClickListener);


        }


    }
}
