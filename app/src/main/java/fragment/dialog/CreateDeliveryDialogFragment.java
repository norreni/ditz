package fragment.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.util.Date;

import backend.ApiRequest;
import entities.Delivery;
import entities.SessionManager;
import entities.User;
import it.ditz.app.MainActivity;
import it.ditz.app.R;
import neomorphism.SegmentItmesNeomorph;
import params.com.stepview.StatusViewScroller;
import tools.MyDialog;
import tools.Utils;

public class CreateDeliveryDialogFragment extends DialogFragment {

    MyDialog myDialog;
    int currentStep = 0;
    Delivery delivery;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        myDialog = new MyDialog(getContext(), R.layout.fragment_create_delivery,null);


        myDialog.dialog.findViewById(R.id.contentDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
        myDialog.dialog.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateDeliveryDialogFragment.this.dismiss();
            }
        });

        myDialog.dialog.findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Utils.isNetworkAvailable())
                {
                    Toast.makeText(getContext(),R.string.error_message_no_network,Toast.LENGTH_LONG).show();
                    return;
                }

                ApiRequest.createDelivery(getContext(), delivery, true, new ApiRequest.Response() {
                    @Override
                    public void onComplete(Object value) {
                        SessionManager.getInstance(getContext()).currentUser.getMyDelieveris().add(0,delivery);
                        CreateDeliveryDialogFragment.this.dismiss();
                        ((MainActivity)getActivity()).reloadData();
                    }

                    @Override
                    public void onError(Object value) {
                        Toast.makeText(getContext(),R.string.error_message_general,Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        myDialog.dialog.getWindow().getDecorView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateDeliveryDialogFragment.this.dismiss();
            }
        });



        myDialog.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;



        /*for(int i = 0;i<viewFlipper.getChildCount();i++){
            View child = viewFlipper.getChildAt(i);
            View next = child.findViewById(R.id.rightArrow);
            View prev = child.findViewById(R.id.leftArrow);
            next.setOnClickListener(this);
            prev.setOnClickListener(this);
        }*/
        User user = SessionManager.getInstance(getContext()).currentUser;

        delivery = new Delivery();
        delivery.setUidOfferer(user.getUid());
        delivery.setUserOfferer(user);
        delivery.setTimeCreate(System.currentTimeMillis());
        delivery.setType(Delivery.Type.market);
        delivery.setLimitPieces(5);
        delivery.setLimitPrice(20);


        SegmentItmesNeomorph segmentCat = myDialog.dialog.findViewById(R.id.segmentCategory);
        SegmentItmesNeomorph segmentPrice = myDialog.dialog.findViewById(R.id.segmentPrice);
        SegmentItmesNeomorph segmentPz = myDialog.dialog.findViewById(R.id.segmentPz);

        segmentCat.onSegmentsItemSelected = new SegmentItmesNeomorph.OnSegmentsItemSelected() {
            @Override
            public void onItemSelected(int index) {
                delivery.setType(index == 0 ? Delivery.Type.market : Delivery.Type.Pharmacy);
            }
        };

        segmentPrice.onSegmentsItemSelected = new SegmentItmesNeomorph.OnSegmentsItemSelected() {
            @Override
            public void onItemSelected(int index) {
                delivery.setLimitPrice(20 * (index + 1));
            }
        };

        segmentPz.onSegmentsItemSelected = new SegmentItmesNeomorph.OnSegmentsItemSelected() {
            @Override
            public void onItemSelected(int index) {
                delivery.setLimitPieces(5 * (index + 1));
            }
        };
        final EditText editWhere = myDialog.dialog.findViewById(R.id.editWhere);
        editWhere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                delivery.setWhereInfo(editWhere.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        final SingleDateAndTimePicker singleDateAndTimePicker = (SingleDateAndTimePicker) myDialog.dialog.findViewById(R.id.timePiker);
        singleDateAndTimePicker.addOnDateChangedListener(new SingleDateAndTimePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                delivery.setTimeExecution(date.getTime());
            }
        });




        return myDialog.dialog;
    }



}
