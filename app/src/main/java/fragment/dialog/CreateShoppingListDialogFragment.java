package fragment.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import backend.ApiRequest;
import entities.SessionManager;
import entities.ShoppingList;
import entities.User;
import it.ditz.app.MainActivity;
import it.ditz.app.R;
import tools.MyDialog;
import tools.Utils;

public class CreateShoppingListDialogFragment extends DialogFragment {

    MyDialog myDialog;
    int currentStep = 0;
    ShoppingList shoppingList;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        myDialog = new MyDialog(getContext(), R.layout.fragment_create_shopping_list,null);


        myDialog.dialog.findViewById(R.id.contentDialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        });
        myDialog.dialog.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateShoppingListDialogFragment.this.dismiss();
            }
        });

        myDialog.dialog.findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Utils.isNetworkAvailable())
                {
                    Toast.makeText(getContext(),R.string.error_message_no_network,Toast.LENGTH_LONG).show();
                    return;
                }

                ApiRequest.createShoppingList(getContext(), shoppingList, true, new ApiRequest.Response() {
                    @Override
                    public void onComplete(Object value) {
                        SessionManager.getInstance(getContext()).currentUser.getMyShoppingLists().add(0, shoppingList);
                        CreateShoppingListDialogFragment.this.dismiss();
                        ((MainActivity)getActivity()).reloadData();
                    }

                    @Override
                    public void onError(Object value) {
                        Toast.makeText(getContext(),R.string.error_message_general,Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        myDialog.dialog.getWindow().getDecorView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateShoppingListDialogFragment.this.dismiss();
            }
        });



        myDialog.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;



        /*for(int i = 0;i<viewFlipper.getChildCount();i++){
            View child = viewFlipper.getChildAt(i);
            View next = child.findViewById(R.id.rightArrow);
            View prev = child.findViewById(R.id.leftArrow);
            next.setOnClickListener(this);
            prev.setOnClickListener(this);
        }*/
        User user = SessionManager.getInstance(getContext()).currentUser;

        shoppingList = new ShoppingList();
        shoppingList.setTimeCreate(System.currentTimeMillis());
        shoppingList.setId(user.getUid()+"_"+ shoppingList.getTimeCreate());
        shoppingList.setUid(user.getUid());


        final EditText editWhere = myDialog.dialog.findViewById(R.id.editWhere);
        editWhere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                shoppingList.setItems(editWhere.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        TextView dialoTitle = myDialog.dialog.findViewById(R.id.dialoTitle);
        dialoTitle.setText(Utils.getTimeFormat(shoppingList.getTimeCreate(),"E MMMM dd, hh:mm a"));

        return myDialog.dialog;
    }



}
