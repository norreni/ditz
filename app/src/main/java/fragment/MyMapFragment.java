package fragment;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.Random;

import backend.ApiRequest;
import entities.Delivery;
import entities.SessionManager;
import entities.User;
import fragment.dialog.DeliveryDetailDialogFragment;
import it.ditz.app.MainActivity;
import it.ditz.app.R;
import neomorphism.MyNeomorphFrameLayout;
import tools.Utils;

public class MyMapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;

    ArrayList<Delivery> allDeliveries = new ArrayList<>();
    ArrayList<Delivery> deliveries = new ArrayList<>();
    ArrayList<Marker> markers = new ArrayList<>();

    BitmapDescriptor bitmapDescriptorFood,bitmapDescriptorHospital;

    Handler handler = new Handler();
    Runnable runnable;

    boolean foodActive = true;
    boolean pharmaActive = true;

    boolean firstLoad = true;


    public MyMapFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);


        // Gets the MapView from the XML layout and creates it
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        runnable = new Runnable() {
            @Override
            public void run() {
                if(getContext()== null)
                    return;
                reloadDeliveries();
                handler.postDelayed(runnable,5 * 1000);
            }
        };
        handler.postDelayed(runnable,100);

        rootView.findViewById(R.id.contentCenterMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusMyPosition();
            }
        });

        rootView.findViewById(R.id.buttonPinFood).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyNeomorphFrameLayout frameLayout = (MyNeomorphFrameLayout) view;
                frameLayout.switchShadowType();
                foodActive = !foodActive;
                loadMarkersDeliveries();
            }
        });

        rootView.findViewById(R.id.buttonPinPharma).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyNeomorphFrameLayout frameLayout = (MyNeomorphFrameLayout) view;
                frameLayout.switchShadowType();
                pharmaActive = !pharmaActive;
                loadMarkersDeliveries();
            }
        });



        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    }

    public void reloadDeliveries(){
        if(getContext()==null)
            return;
        if(Utils.isNetworkAvailable())
            ApiRequest.getDeliveries(getContext(), false, new ApiRequest.Response() {
                @Override
                public void onComplete(Object value) {
                    if(getActivity() == null)
                        return;

                    allDeliveries.clear();
                    allDeliveries.addAll((ArrayList<Delivery>) value);
                    SessionManager.getInstance(getContext()).currentUser.setMyDelieveris(allDeliveries);
                    loadMarkersDeliveries();

                    ((MainActivity)getActivity()).changeStateDeliveryPending();
                    Delivery deliveryInProgress = SessionManager.getInstance(getContext()).currentUser.getDeliveryInProgress();
                    if(deliveryInProgress!= null)
                        updateDialogDetailifOpen(deliveryInProgress);
                    firstLoad = false;
                }

                @Override
                public void onError(Object value) {

                }
            });
    }


    public void focusMyPosition(){
        User user =  SessionManager.getInstance(getContext()).currentUser;
        LatLng myPosition = new LatLng(user.getLatitude(), user.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition,mMap.getCameraPosition().zoom));
    }

    public void loadMarkersDeliveries(){
        if(mMap == null)
            return;
        for(Marker marker : markers)
            marker.remove();

        final User user = SessionManager.getInstance(getContext()).currentUser;
        deliveries.clear();
        for(Delivery delivery : allDeliveries){
            if(!user.getUid().equals(delivery.getUidOfferer())) {
                if (delivery.getType() == Delivery.Type.market && foodActive)
                    deliveries.add(delivery);
                else if (delivery.getType() == Delivery.Type.Pharmacy && pharmaActive)
                    deliveries.add(delivery);
            }
        }
        markers.clear();

        Random r = new Random();

        for(Delivery delivery : deliveries){
            LatLng position = new LatLng(delivery.getLatitude(), delivery.getLongitude());
            for (Marker otherMarker : markers) {
                LatLng pos = otherMarker.getPosition();

                Location loc1 = new Location("");
                loc1.setLatitude(pos.latitude);
                loc1.setLongitude(pos.longitude);

                Location loc2 = new Location("");
                loc2.setLatitude(position.latitude);
                loc2.setLongitude(position.longitude);

                float distanceInMeters = loc1.distanceTo(loc2);

                //if a marker already exists in the same position as this marker
                if (distanceInMeters<10) {
                    int meters = r.nextInt(20 - 11) + 10;
                    int angle = r.nextInt(90 - 1) + 0;
                    position =  SphericalUtil.computeOffset(position,meters,angle);
                    break;
                }
            }

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(position)
                    //.title("Sydney")
                    //.snippet("Population: 4,627,300")
                    .icon(delivery.getType() == Delivery.Type.market ? bitmapDescriptorFood : bitmapDescriptorHospital));
            marker.setTag(delivery);
            markers.add(marker);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.clear();

        int height = 170;
        int width = 170;
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker_food);
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        /*Paint paint = new Paint();
        ColorFilter filter = new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.colorFood), PorterDuff.Mode.SRC_IN);
        paint.setColorFilter(filter);

        Canvas canvas = new Canvas(smallMarker);
        canvas.drawBitmap(smallMarker, 0, 0, paint);*/
        bitmapDescriptorFood = BitmapDescriptorFactory.fromBitmap(smallMarker);

        b = BitmapFactory.decodeResource(getResources(), R.drawable.ic_marker_pharmacy);
        smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        /*paint = new Paint();
        filter = new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.colorPharma), PorterDuff.Mode.SRC_IN);
        paint.setColorFilter(filter);

        canvas = new Canvas(smallMarker);
        canvas.drawBitmap(smallMarker, 0, 0, paint);*/
        bitmapDescriptorHospital = BitmapDescriptorFactory.fromBitmap(smallMarker);


        User user =  SessionManager.getInstance(getContext()).currentUser;
        LatLng myPosition = new LatLng(user.getLatitude(), user.getLongitude());

        /*try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.map_style));

        } catch (Resources.NotFoundException e) {

        }*/


        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Delivery delivery = (Delivery) marker.getTag();
                showDetailDelivery(delivery);
                return false;
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition,17));

        double radiusInMeters = 100.0;
        //red outline
        int strokeColor = getResources().getColor(R.color.colorAccent);
        //opaque red fill
        int shadeColor = getResources().getColor(R.color.colorMapAlpha);


        CircleOptions circleOptions = new CircleOptions().center(myPosition).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(2);
        mMap.addCircle(circleOptions);
    }

    DeliveryDetailDialogFragment createDeliveryDialogFragment;
    public void showDetailDelivery(Delivery delivery){
        createDeliveryDialogFragment = new DeliveryDetailDialogFragment(delivery);
        FragmentTransaction fragmentTransaction =  getChildFragmentManager().beginTransaction();
        Fragment prev = getChildFragmentManager().findFragmentByTag(createDeliveryDialogFragment.getClass().getName());
        if (prev != null) {
            //fragmentTransaction.remove(prev)
            return;
        }
        fragmentTransaction.addToBackStack(null);
        createDeliveryDialogFragment.show(fragmentTransaction,createDeliveryDialogFragment.getClass().getName());
    }

    public void updateDialogDetailifOpen(Delivery delivery){
        if(createDeliveryDialogFragment != null && createDeliveryDialogFragment.isAdded()){
            createDeliveryDialogFragment.delivery = delivery;
            createDeliveryDialogFragment.reloadRecap();
        }
    }
}
